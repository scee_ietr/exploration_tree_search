# Presentation

This repository provides the source code to produce the figures presented for an article under review. The reference will be updated when available.

The code for K-Best, Best-First and geometrical detector are out of the scope of this repository since the first two ones are well-known and the article dealing we the last one is still under reviews. 

# Files description

 * `requirements.txt` lists the required packages in a pip compatible format.
 * `WiMAX_1440.720.txt` specifies the 720,1440 LDPC code from WiMAX standard in a CommPy compatible format.
 * `firefly.py` implements the proposed algorithm and useful helper functions.
 * `boxplot_reuse_nodes.py` gives the code to generate the boxplot studying the number of unique nodes visited.
 * `Monte-Carlo_simu` illustrates the simulation setup on a few example.
 * `soft-4x4.pkl` contains all the data used in the Pareto analysis.
 * `Pareto_analysis` provides the code to process the database and display the Pareto analysis. 

# Database information

`soft-4x4.pkl` contains a list of dict where each dict stores a simulation. Here is a description of the dict keys.

 * Related to the link model:
     * `chanl_label`  : channel fading used.
     * `modem`        : CommPy modem object.
     * `code`         : description of the code used.
     * `decoder`      : decoding algorithm. `MSA-15` stands for Min-Sum Algorithm 15 iterations.
     * `receiver`     : description of the receiver. See bellow for name explanations
        * `IL-L2E-d (n it) (sf=(s))` refers to geometrical detectors with `d` dimensions, `n` local search iterations and scaling factors `s`.
        * `KSE-K` refers to K-best detectors with parameters `K`.
        * `BF(sizes)` refers to cross-level best-first with pool sizes `sizes`.
        * `FA F it, k=k, g=g` refers to the proposed algorithm with `F` fireflies/paths, k=`k` and gamma=`g`. `reuse nodes` means that nodes are computed only once.
 * Related to the Monte-Carlo simulation:
     * `nb_it_max`    : number of bits send before stopping the Monte-Carlo simulation.
     * `nb_err`       : number of errors before stopping the Monte-Carlo simulation.
 * Related to the results
     * `SNRs`         : SNRs in dB.
     * `BERs`         : corresponding bit error ratios.
     * `Complexity`   : tuple (number of product, number of addition). For Best-Fist, it is a tuple of 2 ndarrays with the number of operation per corresponding SNRs.

# Licence
Copyright 2020 SCEE/IETR UMR CNRS 6164, CentraleSupélec

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
