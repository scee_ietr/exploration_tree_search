from pickle import load
from matplotlib.pyplot import semilogy, legend, grid, show, xlabel, ylabel, bar, figure, subplots, xticks, scatter, plot, title
from math import log10
from numpy import min, max, interp, linspace, array

# Global parameters
bers_required = 1e-4  # BER required when computed SNR


def preprocess_data(data):
    snr_required = []
    complexity = []
    receiver_label = []

    # Find SNR required to reach asked BER
    for d in data:
        if d['BERs'][-1] < bers_required:
            # Build a SNR linspace between the SNR just above and just below the SNR we look for
            snr_sup = d['BERs'].size - d['BERs'][::-1].searchsorted(bers_required)
            snr_space = linspace(d['SNRs'][snr_sup - 1], d['SNRs'][snr_sup])

            # Linear interpolation of the BERs
            ber_interp = interp(snr_space,
                                (d['SNRs'][snr_sup - 1], d['SNRs'][snr_sup]),
                                (d['BERs'][snr_sup - 1], d['BERs'][snr_sup]))

            # Find right snr and store complexity and receiver label
            snr_required.append(snr_space[ber_interp.size - ber_interp[::-1].searchsorted(bers_required)])
            complexity.append(log10(min(d['complexity'][0])))
            receiver_label.append(d["receiver"])

    return snr_required, complexity, receiver_label


def compute_pareto(snr_required, complexity, receiver_label):
    # Initialization : sorting by increasing efficiency
    ordering = array(snr_required).argsort()
    snr_required_front = [snr_required[ordering[0]]]
    complexity_front = [complexity[ordering[0]]]
    receiver_label_front = [receiver_label[ordering[0]]]

    # Compute Pareto front
    for idx in ordering[1:]:
        if complexity[idx] < complexity_front[-1]:
            if snr_required[idx] == snr_required_front[-1]:
                del snr_required_front[-1]
                del complexity_front[-1]
                del receiver_label_front[-1]
            snr_required_front.append(snr_required[idx])
            complexity_front.append(complexity[idx])
            receiver_label_front.append(receiver_label[idx])

    return snr_required_front, complexity_front, receiver_label_front


def plot_pareto(data):
    colors = {'KS': 'khaki', 'BF': 'lime', 'IL': 'magenta', 'FA': 'dodgerblue'}
    labels = {'KS': 'K-best', 'BF': 'Best-first', 'IL': 'IL Geometrical', 'FA': 'Firefly'}
    fig, ax = subplots()

    # Add all points
    snr_required, complexity, receiver_label = preprocess_data(data)
    this_colors = [colors[label[:2]] for label in receiver_label]
    scatter(snr_required, complexity, c=this_colors, alpha=.3, edgecolor='k')

    # Add front
    snr_required_front, complexity_front, receiver_label_front = compute_pareto(snr_required, complexity, receiver_label)
    this_colors = [colors[label[:2]] for label in receiver_label_front]
    plot(snr_required_front, complexity_front, 'k--', label='Pareto front')
    scatter(snr_required_front, complexity_front, c=this_colors, marker='^', s=200, edgecolor='k')

    # Trick for the legend
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    to_legend = {(colors[label[:2]], labels[label[:2]]) for label in receiver_label}
    scatter(0, 0, marker='^', c='w', edgecolor='k', label='Pareto efficient')
    for col, lab in to_legend:
        scatter(0, 0, c=col, label=lab)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    xlabel(f'SNRs required for BER = {bers_required:.1e}')
    ylabel('$log_{10}$(number of products)')
    legend(loc='upper right')
    grid()
    show()

    # Print Pareto front label
    print('Pareto frontier for number of products')
    for label in receiver_label_front:
        print(label)


if __name__ == "__main__":
    # Load data from database
    with open('soft-4x4.pkl', 'rb') as data_file:
        data = load(data_file)
        print(f'Loaded {len(data)} experiments')

    # Plot Pareto analysis
    plot_pareto(data)
