from math import log2

from commpy.channelcoding.ldpc import triang_ldpc_systematic_encode, get_ldpc_code_params
from commpy.channels import MIMOFlatChannel
from commpy.modulation import QAMModem
from matplotlib.pyplot import boxplot, figure, grid, xlabel, show
from numpy import empty
from numpy.random import choice

from firefly import ratio_diff_nodes_firefly

"""
This script plot the ratio of different nodes visited during a typical FA detection.
"""

# FA parameters to test
nb_its = 275, 275, 130,
ks = 3, 3, 3,
gammas = 2 ** -6, 2 ** -3, 2 ** -3

# Parameters
nb_bloc = 2
ldpc = get_ldpc_code_params('WiMAX_1440.720.txt')
qam = QAMModem(16)

# Channel
chan = MIMOFlatChannel(4, 4)
chan.uncorr_rayleigh_fading(complex)
chan.set_SNR_dB(17)

# Create symbol vectors and propagate them
encoded = triang_ldpc_systematic_encode(choice((0, 1), 720 * nb_bloc), ldpc).reshape(-1)
symbols = qam.modulate(encoded)
received = chan.propagate(symbols)

# Evaluate the ratio
ratios = empty((len(received), len(nb_its)))
for conf_idx, nb_it, gamma, k in zip(range(len(nb_its)), nb_its, gammas, ks):
    for idx in range(len(received)):
        ratios[idx, conf_idx] = ratio_diff_nodes_firefly(received[idx], chan.channel_gains[idx], qam.constellation,
                                                         nb_it, gamma, k)

# Plot the distribution
figure()
labels = [f'$F={nb_it}$, $log_ 2(\gamma)={int(log2(gamma))}$' for nb_it, gamma in zip(nb_its, gammas)]
boxplot(ratios * 100, vert=False, labels=labels, whis='range', widths=.8)
grid()
xlabel('Ratio of the number of different nodes to the total number of visited nodes')
show()
