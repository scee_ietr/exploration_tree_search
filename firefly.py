from math import log2, sqrt
from commpy import max_log_approx
from commpy.channelcoding.ldpc import triang_ldpc_systematic_encode, get_ldpc_code_params
from commpy.channels import MIMOFlatChannel
from commpy.modulation import QAMModem
from numpy import block, concatenate, array, unique, int8, zeros, cumsum, sum, empty
from numpy.linalg import qr
from numpy.random import choice, random


def firefly(y, h, constellation, nb_iter=275, gamma=0.5, k=1, noise_var=0., output_type='hard', demode=None):
    """ MIMO Firefly Algorithm Detection. This detector is suitable for QAM.

        Parameters
        ----------
        y : 1D ndarray
            Received complex symbols (length: num_receive_antennas)

        h : 2D ndarray
            Channel Matrix (shape: num_receive_antennas x num_transmit_antennas)

        constellation : 1D ndarray of floats or complex
            Constellation used to modulate the symbols

        nb_iter : positive integer
            Number of iterations.
            **Default:** 275

        gamma : positive float
            Absorption coefficient.
            **Default:** 0.5

        k : positive float
            Exponent coefficient for computing the attractiveness.
            *Default* value is 1.

        noise_var : positive float
            Noise variance.
            *Default* value is 0.

        output_type : str
            'hard': hard output i.e. output is a binary word
            'soft': soft output i.e. output is a vector of Log-Likelihood Ratios.
            *Default* value is 'hard'

        demode : function with prototype binary_word = demode(point)
            Function that provide the binary word corresponding to a symbol vector.

        Returns
        -------
        x : 1D ndarray of constellation points or of Log-Likelihood Ratios.
            Detected vector (length: num_receive_antennas).
    """

    # keep a copy of y and h because we need them for soft-output
    H = h.copy()
    Y = y.copy()

    # transform channel matrix h, y and constellation as real matrix
    if isinstance(h[0, 0], complex):
        h = block([[h.real, -h.imag], [h.imag, h.real]])
        y = concatenate((y.real, y.imag))
        constellation = array(unique(constellation.real))
        is_complex = True
    else:
        is_complex = False

    # number of transmit antennas & receive antennas
    nb_tx, nb_rx = h.shape
    N = nb_tx

    # allocate memory for vectors
    x = empty((nb_iter, N), dtype=int8)

    # QR decomposition
    q, r = qr(h)
    yt = q.T.dot(y)

    # allocate memory for E
    E = zeros(nb_iter)

    for i in range(N - 1, -1, -1):
        # compute the Euclidean distance
        sum_temp = sum(r[i, i + 1:] * x[:, i + 1:], axis=1)

        # make assumptions
        ud = (yt[i] - r[i, i] * constellation - sum_temp[:, None]) ** 2

        # compute attractiveness parameter
        beta = 1 / (1 + gamma * ud ** k)

        # Compute cumulative distribution function
        cdf = cumsum(beta / beta.sum(1)[:, None], 1)

        # generate uniformly random variable called alpha
        alpha = random(nb_iter)

        # calculate xi value (equ 19)
        for it in range(nb_iter):
            x[it, i] = constellation[cdf[it].searchsorted(alpha[it])]

        # update E
        E += (yt[i] - r[i, i] * x[:, i] - sum_temp) ** 2

    if output_type == 'hard':
        x_opt = x[E.argmin()]
        if is_complex:
            return x_opt[:N // 2] + 1j * x_opt[N // 2:]
        else:
            return x_opt

    elif output_type == 'soft':
        if is_complex:
            x = x[:, :N // 2] + 1j * x[:, N // 2:]
        return max_log_approx(Y, H, noise_var, x.transpose(), demode)
    else:
        raise ValueError('output_type must be "hard" or "soft"')


def ratio_diff_nodes(nodes):
    """
    Count the ratio of different explored nodes from a list of nodes in tree paths.

    Parameters
    ----------
    nodes: 2d-ndarray
        Nodes list with nodes[i] being the i-th node.

    Return
    ------
    ratio: float
        The ratio of the number of different nodes over the total number of visited nodes.

    """

    nb_nodes = nodes.size

    for idx, path in enumerate(nodes):
        possible_paths = list(nodes[:idx])
        for coord, val in enumerate(path):
            possible_paths = [p for p in possible_paths if p[coord] == val]
            nb_nodes -= len(possible_paths) > 0

    return nb_nodes / nodes.size


def ratio_diff_nodes_firefly(y, h, constellation, nb_iter=275, gamma=0.5, k=1):
    """
    Count the average ratio of different explored nodes for a firefly detection.

    Parameters
    ----------
    y : 1D ndarray
        Received complex symbols (length: num_receive_antennas)

    h : 2D ndarray
        Channel Matrix (shape: num_receive_antennas x num_transmit_antennas)

    constellation : 1D ndarray of floats or complex
        Constellation used to modulate the symbols

    nb_iter : positive integer
        Number of iterations.
        **Default:** 275

    gamma : positive float
        Absorption coefficient.
        **Default:** 0.5

    k : positive float
        Exponent coefficient for computing the attractiveness.
        *Default* value is 1.

    Return
    ------
    ratio: float
        The ratio of the number of different nodes over the total number of visited nodes.
    """

    # transform channel matrix h, y and constellation as real matrix
    if isinstance(h[0, 0], complex):
        h = block([[h.real, -h.imag], [h.imag, h.real]])
        y = concatenate((y.real, y.imag))
        constellation = array(unique(constellation.real))
        is_complex = True
    else:
        is_complex = False

    # number of transmit antennas & receive antennas
    nb_tx, nb_rx = h.shape
    N = nb_tx

    # allocate memory for vectors
    x = empty((nb_iter, N), dtype=int8)

    # QR decomposition
    q, r = qr(h)
    yt = q.T.dot(y)

    # allocate memory for E
    E = zeros(nb_iter)

    for i in range(N - 1, -1, -1):
        # compute the Euclidean distance
        sum_temp = sum(r[i, i + 1:] * x[:, i + 1:], axis=1)

        # make assumptions
        ud = (yt[i] - r[i, i] * constellation - sum_temp[:, None]) ** 2

        # compute attractiveness parameter
        beta = 1 / (1 + gamma * ud ** k)

        # Compute cumulative distribution function
        cdf = cumsum(beta / beta.sum(1)[:, None], 1)

        # generate uniformly random variable called alpha
        alpha = random(nb_iter)

        # calculate xi value (equ 19)
        for it in range(nb_iter):
            x[it, i] = constellation[cdf[it].searchsorted(alpha[it])]

        # update E
        E += (yt[i] - r[i, i] * x[:, i] - sum_temp) ** 2

    return ratio_diff_nodes(x[:, :N // 2] + 1j * x[:, N // 2:])


def average_gain(nb_it, k, gamma, mod_order, nb_bloc):
    """
    Compute the average gain when reusing nodes in FA-based detector.

    Parameters
    ----------
    nb_it : positive integer
        Number of FA iterations.

    k : positive float
        Exponent coefficient for computing the attractiveness.

    gamma : positive float
        Absorption coefficient.

    mod_order : positive integer
        Modulation order, i.e. number of bits per symbols.

    nb_bloc : positive integer
        Number of LDPC blocs to average the result on.

    Return
    ------
    ratio: float
        The ratio of the number of different nodes over the total number of visited nodes.
    """
    gain = 0
    nb_gains = 0

    ldpc = get_ldpc_code_params('WiMAX_1440.720.txt')
    encoded = triang_ldpc_systematic_encode(choice((0, 1), 720 * nb_bloc), ldpc).reshape(-1)
    qam = QAMModem(2 ** mod_order)
    symbols = qam.modulate(encoded)

    chan = MIMOFlatChannel(4, 4)
    chan.uncorr_rayleigh_fading(complex)
    chan.set_SNR_dB(19)

    received = chan.propagate(symbols)
    for y, h in zip(received, chan.channel_gains):
        gain += ratio_diff_nodes_firefly(y, h, qam.constellation, nb_it, gamma, k)
        nb_gains += 1

    return gain / nb_gains


def FA_complexity(n, nb_iter, iscomplex, constellation, gamma, k=1, bits_per_alpha=8, output_type='hard'):
    """ FA number of products and additions for a square MIMO.

    We assume that:
        * the random variable is obtained through a linear-feedback shift register.
        * a division is equal to a product.
        * the total computational is equivalent to the cost of evaluating the nodes objective function.
        * reuse ratio is computed at 19 dB.

    Parameters
    ----------
    n : positive integer
        Number of dimensions (twice the number of antennas is the channel is complex)

    nb_iter : positive integer
        Number of iterations

    iscomplex : bool
        Is the communication using complex?

    constellation : 1D ndarray of floats or complex
        Constellation used to modulate the symbols

    gamma : positive float
        Absorption coefficient.

    k : positive integer
        Exponent coefficient for computing the attractiveness.
        *Default* value is 1.

    bits_per_alpha : positive integer
        Number of bit to represent alpha. Used to compute how many linear-feedback shift register values are required.
        *Default* is 8 bits.

    output_type : str
            'hard': hard output i.e. output is a binary word
            'soft': soft output i.e. output is a vector of Log-Likelihood Ratios.
            *Default* value is 'hard'

    Return
    ------
    nb_prod, nb_add : tuple of 2 ints or tuple of 2 ndarray
        Number of real products and additions
    """
    if iscomplex:
        const = sqrt(constellation.size)
    else:
        const = constellation.size

    # Initialisation
    nb_prod = 0
    nb_add = 0

    # yt assuming pre-processing already computed the QR decomposition
    nb_prod += n ** 2
    nb_add += (n - 1) * n

    # For each coordinates
    for i in range(n - 1, -1, -1):
        # compute the euclidean distance
        nb_prod += (n - 1 - i) * nb_iter
        nb_add += (n - 1 - i) * nb_iter

        # make assumptions
        nb_prod += 2 * const * nb_iter
        nb_add += 2 * const * nb_iter

        # compute attractiveness parameter: 1 / (1 + gamma * ud ** k)
        nb_prod += k * const * nb_iter
        nb_add += nb_iter * const

        # Compute cumulative distribution function
        nb_prod += nb_iter
        nb_add += const * 2 * nb_iter

        # generate uniformly random variable called alpha
        nb_add += nb_iter * bits_per_alpha

        # calculate xi value (equ 19)
        pass

        # update E
        nb_prod += 2 * nb_iter

    if output_type == 'hard':
        if iscomplex:
            # rebuild complex x_opt
            nb_prod += n // 2
            nb_add += n // 2

    if output_type == 'soft':
        if iscomplex:
            # Difference of the minimums
            nb_add += n * log2(constellation.size) / 2  # /2 due to real-valued model

            # Divide by noise_var
            nb_prod += n * log2(constellation.size) / 2  # /2 due to real-valued model
        else:
            # Difference of the minimums
            nb_add += n * log2(constellation.size)

            # Divide by noise_var
            nb_prod += n * log2(constellation.size)

    average_ratio = average_gain(nb_iter, k, gamma, log2(constellation.size), 2)
    return nb_prod * average_ratio, nb_add * average_ratio
