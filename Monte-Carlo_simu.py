###########################################
# Control multi-threading
#
# Multi-threading is not recommended with multiprocessing.
###########################################
import os

os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_NUM_THREADS'] = '1'
num_cpus = 3

###########################################
# Imports
###########################################

from math import ceil
from commpy.channelcoding.ldpc import get_ldpc_code_params, triang_ldpc_systematic_encode, ldpc_bp_decode
from commpy.channels import MIMOFlatChannel
from commpy.modulation import QAMModem
from commpy.links import LinkModel, link_performance
from firefly import firefly, FA_complexity
from numpy import arange
from pickle import load, dump
from shutil import copy2
from p_tqdm import p_map

############################################
# Saving
############################################
file_name = 'soft-4x4'
save = False

############################################
# LDPC code
############################################
ldpc_params = get_ldpc_code_params('WiMAX_1440.720.txt', True)

############################################
# Model
############################################
modem = QAMModem(16)
channel = MIMOFlatChannel(4, 4)
channel.uncorr_rayleigh_fading(complex)

# Same SNRs for every model
SNRs = arange(0, 25, .5)

############################################
# Modulate & demodulate
############################################
modulate = lambda bits: modem.modulate(triang_ldpc_systematic_encode(bits, ldpc_params, False).reshape(-1, order='F'))
demode = lambda symbs: modem.demodulate(symbs, 'hard')


def decoder(llrs):
    return ldpc_bp_decode(llrs, ldpc_params, 'MSA', 15)[0][:720].reshape(-1, order='F')


############################################
# Detectors
############################################
receivers = []
receivers_str = []
complexity = []

nb_its = 275,
ks = 3,
gammas = 0.5, 1

receivers += [
    (lambda y, h, constellation, noise_var, nb_it=nb_it, k=k, gamma=gamma:
     firefly(y, h, constellation, nb_it, gamma, k, noise_var, 'soft', demode))
    for nb_it in nb_its
    for k in ks
    for gamma in gammas]

receivers_str += ['FA {} it, k={}, g={}'.format(nb_it, k, gamma)
                  for nb_it in nb_its
                  for k in ks
                  for gamma in gammas]

complexity += [FA_complexity(2 * channel.nb_tx, nb_it, True, modem.constellation, gamma, k, 8, 'soft')
               for nb_it in nb_its
               for k in ks
               for gamma in gammas]

############################################
# Build tuples
############################################
nb_test = len(receivers)

modulates = (modulate,) * nb_test
demodes = (demode,) * nb_test
modems = (modem,) * nb_test
channels = (channel,) * nb_test
decoders = (decoder,) * nb_test
chan_label = ('Uncorr Rayleigh',) * nb_test
decoders_str = ('MSA-15',) * nb_test
chan_label = ('Uncorr Rayleigh',) * nb_test
code_str = ('LDPC WiMAX 1440-720',) * nb_test
ldpc_params_list = (ldpc_params,) * nb_test
code_rate = .5

############################################
# Link_performance
############################################
nb_err = 200
nb_it = ceil(nb_err / 4e-2)
chunk = 1440

###########################################
# File managment
###########################################
if save:
    data_file_name = file_name + '.pkl'
    ###########################################
    # Backup or create the pickle file
    ###########################################
    try:
        copy2(data_file_name, data_file_name + '.old')
    except FileNotFoundError:
        open(data_file_name, 'x').close()

    ############################################
    # Read previous tests if any
    ############################################
    with open(data_file_name, 'rb') as file:
        try:
            data = load(file)
        except EOFError:
            data = []

############################################
# Models
############################################
models = []
for i in range(nb_test):
    models.append(LinkModel(modulates[i], channels[i], receivers[i],
                            modems[i].num_bits_symbol, modems[i].constellation, modems[i].Es,
                            decoders[i], code_rate))


############################################
# Test
############################################
def perf(model):
    return link_performance(model, SNRs, nb_it, nb_err, chunk, code_rate)


BERs = p_map(perf, models, num_cpus=num_cpus)
# BERs = tuple(map(perf, models))

if save:
    ###########################################
    # Save new test
    ############################################
    for i in range(nb_test):
        data.append({'modem': modems[i],
                     'channel': channels[i],
                     'SNRs': SNRs,
                     'BERs': BERs[i],
                     'nb_it_max': nb_it,
                     'nb_err': nb_err,
                     'receiver': receivers_str[i],
                     'chan_label': chan_label[i]})
    with open(data_file_name, 'wb') as file:
        dump(data, file)
else:
    # print(complexity)
    from matplotlib.pyplot import semilogy, grid, show, xlabel, ylabel

    for BER in BERs:
        semilogy(SNRs, BER)
    grid()
    xlabel('SNRs')
    ylabel('BERs')
    show()
